{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}
{-# OPTIONS_GHC -cpp -pgmP../genserth/zuker #-}

module Data.Concattic where

import Data.Dynamic
import Data.Word
import qualified Data.Symbol as S

data E = W | U | Q E | H E | E :~ E | E :# E | SW Word | D Dynamic

instance Show E where
  show (SW a) = S.fromSymbol (S.SW a)
  show (W :# a :~ b)   = show b
  show (a :# b :~ o@SW{}) = show a ++ "; " ++ show o
  show (a :# b :~ Q  c) = show a ++ "; " ++ "{ " ++ show c ++ " }"
  show (a :# b :~ D  c) = show a ++ "; " ++ show c
  show (a :# b :~ c)   = show a ++ "; " ++ show c
  show (Q a) = "{ " ++ show a ++ " }"
  show (a :# b) = show a
  show (a :~ b) = show a ++ "; " ++ show b
  show a = "_"


test123 = W :U~ {test} :U~ {best} :U~ Q {test2} :U~ {super} :U~ dyn (123::Integer) :U~ dyn (print :: Integer -> IO ())

testA = W :U~ Q (W :U~ {port} :U~ {port}) :U~ {port} :U~ {swap}
testB = W :U~ Q {portA} :U~ Q {portB} :U~ Q {portC} :U~ {swap}




dyn :: Typeable a => a -> E
dyn a = D $ toDyn a


reduce :: Int -> E -> Maybe E

reduce 1         (a :1~ Q b :2~ {unit}) = Just $ a :12~ Q (Q b)
reduce 1 (a :1~ Q b :2~ Q c :3~ {swap}) = Just $ a :1~ Q c :23~ Q b
reduce 1 (a :1~ Q b :2~ Q c :3~ {cons}) = Just $ a :123~ Q ( Q b :U~ c )
reduce 1 (a :1~ Q b :2~ Q c :3~ {dip} ) = Just $ a :1~ b :23~ Q c

reduce 1 (a :# W) = Just $ a
reduce 1 (a :# (wh1 :~ U)) = Just $ a :# wh1

reduce 1 (a :# h1 :~ Q b :# h2 :~ {hide}) = Just $ a :# (W :~ h1 :~ (H (Q b)) :~ h2)
reduce 1 (a :# (wh1 :~ (H (Q b))) :~ {plug}) = Just $ a :# wh1 :~ Q b
reduce 1 (a :# (H (Q b)) :~ {plug}) = Just $ a :~ Q b

reduce 1 (a :~ b :# U :~ {plug}) = case reduce 1 (a :~ {plug}) of
  Just (c :~ Q d) -> Just $ c :~ b :# U :~ Q d
  _ -> Nothing

reduce 1 a = Nothing







toSymbol a = let (S.SW b) = S.toSymbol a in SW b

test2 = {Super}

test3 {Swap} = "kuku"

test = "         a :12~ b            {ok}       {ob}       "

eval o@(a :# h :~ b) = case reduce 1 o of
  Just c  -> toRight $ eval c
  Nothing -> case (a,eval a,eval (a :# h)) of
    (_,_,Right d) -> eval (d :~ b)
    (_,Right d,_) -> eval (d :# h :~ b)
    (_,Left _,Left _) -> Left o

eval o@(a :~ b) = case reduce 1 o of
  Just c  -> toRight $ eval c
  Nothing -> case (a,eval a) of
    (_,Right d) -> eval (d :~ b)
    (_,Left  e) -> Left o


eval o@(a :# (b :~ c)) = case reduce 1 o of
  Just c  -> toRight $ eval c
  Nothing -> case (b,eval (a :# b),eval a) of
    (_,Right (d :# e),_) -> eval (d :# (e :~ c))
    (_,Right d,_)        -> eval (d :# c)
    (_,_,Right d)        -> eval (d :# (b :~ c))
    _                  -> Left o

eval o@(a :# b) = case reduce 1 o of
  Just c  -> toRight $ eval c
  Nothing -> case eval a of
    Right d -> eval (d :# b)
    Left  e -> Left o

eval a = Left a

toRight (Left  a) = Right a
toRight (Right a) = Right a

fromRight (Right a) = a

